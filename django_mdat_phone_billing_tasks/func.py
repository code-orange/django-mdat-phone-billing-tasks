import decimal

from openpyxl import Workbook

from django_mdat_phone_billing.django_mdat_phone_billing.models import (
    MdatPhoneBillingCountryItems,
    MdatPhoneBillingRouteItems,
)


def mdat_phone_billing_export_country_pricelist():
    wb = Workbook()

    # grab the active worksheet
    ws = wb.active

    ws["A1"] = "Land"
    ws["B1"] = "Festnetz"
    ws["C1"] = "Mobilfunk"

    for country in MdatPhoneBillingCountryItems.objects.all():
        ws.append(
            [
                country.country_code.country.safe_translation_getter(
                    "title", language_code="de"
                ),
                "{} Cent/Min.\n({} Cent/Min. inkl. MwSt.)".format(
                    str(
                        round(
                            country.item_fixed_line.price * decimal.Decimal(100),
                            2,
                        )
                    ).replace(".", ","),
                    str(
                        round(
                            country.item_fixed_line.price
                            * decimal.Decimal(1.19)
                            * decimal.Decimal(100),
                            2,
                        )
                    ).replace(".", ","),
                ),
                "{} Cent/Min.\n({} Cent/Min. inkl. MwSt.)".format(
                    str(
                        round(
                            country.item_mobile.price * decimal.Decimal(100),
                            2,
                        )
                    ).replace(".", ","),
                    str(
                        round(
                            country.item_mobile.price
                            * decimal.Decimal(1.19)
                            * decimal.Decimal(100),
                            2,
                        )
                    ).replace(".", ","),
                ),
            ]
        )

    # Save the file
    wb.save("mdat_phone_billing_export_country_pricelist.xlsx")


def mdat_phone_billing_export_zone_assignment():
    wb = Workbook()

    # grab the active worksheet
    ws = wb.active

    ws["A1"] = "Route-ID"
    ws["B1"] = "Route"
    ws["C1"] = "Artikelnummer"
    ws["D1"] = "Artikelname / Zone"
    ws["E1"] = "Agerechnet pro"
    ws["F1"] = "Identifikation der Route"
    ws["G1"] = "Hinzugefügt am"
    ws["H1"] = "Gültig ab"
    ws["I1"] = "Erreichbar"
    ws["J1"] = "Sekunden kostenlos"
    ws["K1"] = "Preis pro Einheit in Euro (netto)"
    ws["L1"] = "Preis pro Einheit in Euro (brutto)"

    for route in (
        MdatPhoneBillingRouteItems.objects.all()
        .order_by("routing_key")
        .select_related()
    ):
        reachable = True

        if route.item.external_id.startswith(
            "DTELMINS"
        ) and not route.item.external_id.startswith("DTELMINS49"):
            reachable = False

        if route.item.external_id.startswith("DTELMINS4919"):
            reachable = False

        if route.item.external_id.startswith("DTELMINS4912"):
            reachable = False

        if route.item.external_id.startswith("DTELMINS49-118"):
            reachable = False

        if route.item.external_id.startswith("DTELMINS4918"):
            reachable = False

        if route.item.external_id.startswith("DTELMINS49900"):
            reachable = False

        if route.item.external_id.startswith("DTELMINL49138"):
            reachable = False

        if route.item.external_id.startswith("DTELMINS49166"):
            reachable = False

        ws.append(
            [
                str(route.id),
                str(route.routing_key),
                route.item.external_id,
                route.item.name,
                route.get_billing_type_display(),
                route.get_ident_type_display(),
                route.date_added.replace(tzinfo=None),
                route.valid_from,
                reachable,
                route.free_time,
                route.item.price,
                route.item.price * decimal.Decimal(1.19),
            ]
        )

    # Save the file
    wb.save("mdat_phone_billing_export_zone_assignment.xlsx")
