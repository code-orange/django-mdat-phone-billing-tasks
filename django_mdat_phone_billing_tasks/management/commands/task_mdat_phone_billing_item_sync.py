from django.core.management.base import BaseCommand

from django_mdat_phone_billing_tasks.django_mdat_phone_billing_tasks.tasks import (
    mdat_phone_billing_item_sync,
)


class Command(BaseCommand):
    help = "Run task mdat_phone_billing_item_sync"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        mdat_phone_billing_item_sync()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
