from django.core.management.base import BaseCommand

from django_mdat_phone.django_mdat_phone.models import MdatPhoneCountryCode


class Command(BaseCommand):
    help = "Run task mdat_phone_billing_item_sync"

    def handle(self, *args, **options):
        country_codes = MdatPhoneCountryCode.objects.values("country_code")

        for cc_result in country_codes:
            cc = cc_result["country_code"]

            countries = MdatPhoneCountryCode.objects.filter(country_code=cc)

            item_country_name_list = list()

            for country in countries:
                item_country_name_list.append(
                    country.country.safe_translation_getter("title", language_code="de")
                )

            item_country_name = ", ".join(item_country_name_list)

            item_country_name = item_country_name.replace("'", "''")

            item_name_fixed_line = (
                "Telefonie Minute Festnetz (+" + str(cc) + ") " + item_country_name
            )
            item_name_mobile = (
                "Telefonie Minute Mobil (+" + str(cc) + ") " + item_country_name
            )
            item_name_toll_free = (
                "Telefonie Minute Freecall (+" + str(cc) + ") " + item_country_name
            )

            if len(item_name_fixed_line) > 100:
                item_name_fixed_line = item_name_fixed_line[:97] + "..."

            if len(item_name_mobile) > 100:
                item_name_mobile = item_name_mobile[:97] + "..."

            if len(item_name_toll_free) > 100:
                item_name_toll_free = item_name_toll_free[:97] + "..."

            sql1 = (
                "UPDATE dbo.OITM SET ItemName = '"
                + item_name_fixed_line
                + "', UserText = '"
                + item_country_name
                + "', PicturName = NULL WHERE ItemCode = 'DTELMINL"
                + str(cc)
                + "';"
            )
            sql2 = (
                "UPDATE dbo.OITM SET ItemName = '"
                + item_name_mobile
                + "', UserText = '"
                + item_country_name
                + "', PicturName = NULL WHERE ItemCode = 'DTELMINM"
                + str(cc)
                + "';"
            )
            sql3 = (
                "UPDATE dbo.OITM SET ItemName = '"
                + item_name_toll_free
                + "', UserText = '"
                + item_country_name
                + "', PicturName = NULL WHERE ItemCode = 'DTELMINF"
                + str(cc)
                + "';"
            )

            print(sql1)
            print(sql2)
            print(sql3)
