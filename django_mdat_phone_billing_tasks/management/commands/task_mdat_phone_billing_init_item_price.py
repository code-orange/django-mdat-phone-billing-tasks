from django.core.management.base import BaseCommand

from django_mdat_phone_billing_tasks.django_mdat_phone_billing_tasks.tasks import (
    mdat_phone_billing_init_item_price,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        mdat_phone_billing_init_item_price()
