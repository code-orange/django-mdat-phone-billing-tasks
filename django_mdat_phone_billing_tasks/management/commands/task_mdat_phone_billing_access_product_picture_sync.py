from django.core.management.base import BaseCommand

from django_mdat_phone_billing_tasks.django_mdat_phone_billing_tasks.tasks import (
    mdat_phone_billing_access_product_picture_sync,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        mdat_phone_billing_access_product_picture_sync()
