import requests
from cairosvg import svg2png
from celery import shared_task
from django.conf import settings
from django.db.models import Q

from django_mdat_customer.django_mdat_customer.models import MdatItems, MdatItemPrices
from django_mdat_location.django_mdat_location.models import MdatCountries
from django_mdat_phone.django_mdat_phone.models import MdatPhoneCountryCode
from django_mdat_phone_billing.django_mdat_phone_billing.models import (
    MdatPhoneBillingCountryItems,
    MdatPhoneBillingRouteCarrierZones,
    MdatPhoneBillingRouteItems,
)
from django_mdat_prod_info_mgmt.django_mdat_prod_info_mgmt.models import (
    MdatItemPictures,
)


@shared_task(name="mdat_phone_billing_item_sync")
def mdat_phone_billing_item_sync():
    for country_code in MdatPhoneCountryCode.objects.all():
        try:
            country_code_billing = MdatPhoneBillingCountryItems.objects.get(
                country_code=country_code
            )
        except MdatPhoneBillingCountryItems.DoesNotExist:
            country_code_billing = MdatPhoneBillingCountryItems(
                country_code=country_code,
            )

        # item_fixed_line
        item_fixed_line_ext_id = "DTELMINL" + str(country_code.country_code)
        try:
            item_fixed_line = MdatItems.objects.get(
                created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                external_id=item_fixed_line_ext_id,
            )
        except MdatItems.DoesNotExist:
            continue
        else:
            country_code_billing.item_fixed_line = item_fixed_line

        # item_mobile
        item_mobile_line_ext_id = "DTELMINM" + str(country_code.country_code)
        try:
            item_mobile = MdatItems.objects.get(
                created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                external_id=item_mobile_line_ext_id,
            )
        except MdatItems.DoesNotExist:
            continue
        else:
            country_code_billing.item_mobile = item_mobile

        # item_toll_free
        item_toll_free_ext_id = "DTELMINF" + str(country_code.country_code)
        try:
            item_toll_free = MdatItems.objects.get(
                created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                external_id=item_toll_free_ext_id,
            )
        except MdatItems.DoesNotExist:
            continue
        else:
            country_code_billing.item_toll_free = item_toll_free

        country_code_billing.save()

    return


@shared_task(name="mdat_phone_billing_carrier_zones_plusnet_seed")
def mdat_phone_billing_carrier_zones_plusnet_seed():
    for country in MdatCountries.objects.filter(iso3__isnull=False):
        # Countrycode
        countrycodes = country.mdatphonecountrycode_set.all()

        # Skip countries without country code
        if len(countrycodes) <= 0:
            continue

        countrycode = countrycodes.first().country_code

        # Landline
        carrier_code_landline = country.iso3.upper() + "0" + "DEF"
        own_code_landline = "DTELMINL" + str(countrycode)

        try:
            carrier_code_landline_obj = MdatPhoneBillingRouteCarrierZones.objects.get(
                carrier_id=175397,
                carrier_code=carrier_code_landline,
            )
        except MdatPhoneBillingRouteCarrierZones.DoesNotExist:
            carrier_code_landline_obj = MdatPhoneBillingRouteCarrierZones(
                carrier_id=175397,
                carrier_code=carrier_code_landline,
                item=MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_landline,
                ),
            )
            carrier_code_landline_obj.save()

        # Mobile
        carrier_code_mobile = country.iso3.upper() + "1" + "DEF"
        own_code_mobile = "DTELMINM" + str(countrycode)

        try:
            carrier_code_mobile_obj = MdatPhoneBillingRouteCarrierZones.objects.get(
                carrier_id=175397,
                carrier_code=carrier_code_mobile,
            )
        except MdatPhoneBillingRouteCarrierZones.DoesNotExist:
            carrier_code_mobile_obj = MdatPhoneBillingRouteCarrierZones(
                carrier_id=175397,
                carrier_code=carrier_code_mobile,
                item=MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_mobile,
                ),
            )
            carrier_code_mobile_obj.save()

        # Special
        carrier_code_special = country.iso3.upper() + "3" + "SRN"
        own_code_special = "DTELMINS" + str(countrycode)

        try:
            carrier_code_special_obj = MdatPhoneBillingRouteCarrierZones.objects.get(
                carrier_id=175397,
                carrier_code=carrier_code_special,
            )
        except MdatPhoneBillingRouteCarrierZones.DoesNotExist:
            carrier_code_special_obj = MdatPhoneBillingRouteCarrierZones(
                carrier_id=175397,
                carrier_code=carrier_code_special,
                item=MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_special,
                ),
            )
            carrier_code_special_obj.save()

    return


@shared_task(name="mdat_phone_billing_create_items")
def mdat_phone_billing_create_items():
    for country in MdatCountries.objects.all():
        print("Checking: " + country.title)

        # Countrycode
        countrycodes = country.mdatphonecountrycode_set.all()

        # Skip countries without country code
        if len(countrycodes) <= 0:
            continue

        for countrycode_obj in countrycodes:
            countrycode = countrycode_obj.country_code

            # Landline
            own_code_landline = "DTELMINL" + str(countrycode)

            try:
                landline_item = MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_landline,
                )
            except MdatItems.DoesNotExist:
                landline_item = MdatItems(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_landline,
                    linked_to_id=1,
                    name="Telefonie Minute Festnetz (+{}) {}".format(
                        countrycode, country.title
                    ),
                )
                landline_item.save()

            # Mobile
            own_code_mobile = "DTELMINM" + str(countrycode)

            try:
                mobile_item = MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_mobile,
                )
            except MdatItems.DoesNotExist:
                mobile_item = MdatItems(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_mobile,
                    linked_to_id=1,
                    name="Telefonie Minute Mobil (+{}) {}".format(
                        countrycode, country.title
                    ),
                )
                mobile_item.save()

            # Special
            own_code_special = "DTELMINS" + str(countrycode)

            try:
                special_item = MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_special,
                )
            except MdatItems.DoesNotExist:
                special_item = MdatItems(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_special,
                    linked_to_id=1,
                    name="Telefonie Minute Personal, Premium, Shared Cost (+{}) {}".format(
                        countrycode, country.title
                    ),
                )
                special_item.save()

            # Freecall
            own_code_freecall = "DTELMINF" + str(countrycode)

            try:
                freecall_item = MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_freecall,
                )
            except MdatItems.DoesNotExist:
                freecall_item = MdatItems(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_freecall,
                    linked_to_id=1,
                    name="Telefonie Minute Freecall (+{}) {}".format(
                        countrycode, country.title
                    ),
                )
                freecall_item.save()

            # Flat Landline
            own_code_flat_landline = "DTELFLATL" + str(countrycode)

            try:
                flat_landline_item = MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_flat_landline,
                )
            except MdatItems.DoesNotExist:
                flat_landline_item = MdatItems(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_flat_landline,
                    linked_to_id=1,
                    name="Telefonie Flatrate Festnetz pro Kanal (mtl.) (+{}) {}".format(
                        countrycode, country.title
                    ),
                )
                flat_landline_item.save()

            # Flat Mobile
            own_code_flat_mobile = "DTELFLATM" + str(countrycode)

            try:
                flat_mobile_item = MdatItems.objects.get(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_flat_mobile,
                )
            except MdatItems.DoesNotExist:
                flat_mobile_item = MdatItems(
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                    external_id=own_code_flat_mobile,
                    linked_to_id=1,
                    name="Telefonie Flatrate Mobil pro Kanal (mtl.) (+{}) {}".format(
                        countrycode, country.title
                    ),
                )
                flat_mobile_item.save()

    return


@shared_task(name="mdat_phone_billing_init_item_price")
def mdat_phone_billing_init_item_price():
    # Set landline price if none exists
    new_landline_item_prices = (
        MdatItemPrices.objects.filter(item__created_by=settings.MDAT_ROOT_CUSTOMER_ID)
        .filter(item__external_id__startswith="DTELMINL")
        .filter(valid_from="1970-01-01")
        .filter(price=0)
        .exclude(item__external_id__startswith="DTELMINL49")
        .exclude(item__external_id__startswith="DTELMINL800")
    )

    for item_price in new_landline_item_prices:
        item_price.price = 1.99 / 1.19
        item_price.save()

    # Set mobile price if none exists
    new_mobile_item_prices = (
        MdatItemPrices.objects.filter(item__created_by=settings.MDAT_ROOT_CUSTOMER_ID)
        .filter(item__external_id__startswith="DTELMINM")
        .filter(valid_from="1970-01-01")
        .filter(price=0)
    )

    for item_price in new_mobile_item_prices:
        item_price.price = 1.99 / 1.19
        item_price.save()

    # Set special price if none exists
    new_special_item_prices = (
        MdatItemPrices.objects.filter(item__created_by=settings.MDAT_ROOT_CUSTOMER_ID)
        .filter(item__external_id__startswith="DTELMINS")
        .filter(valid_from="1970-01-01")
        .filter(price=0)
        .exclude(item__external_id__startswith="DTELMINS49")
    )

    for item_price in new_special_item_prices:
        item_price.price = 1.99 / 1.19
        item_price.save()

    # Set flatrate price if none exists
    new_flatrate_item_prices = (
        MdatItemPrices.objects.filter(item__created_by=settings.MDAT_ROOT_CUSTOMER_ID)
        .filter(item__external_id__startswith="DTELFLAT")
        .filter(valid_from="1970-01-01")
        .filter(price=0)
    )

    for item_price in new_flatrate_item_prices:
        item_price.price = 99 / 1.19
        item_price.save()

    return


@shared_task(name="mdat_phone_billing_route_items_country_sync")
def mdat_phone_billing_route_items_country_sync():
    for country_code in MdatPhoneCountryCode.objects.all():
        try:
            country_code_billing = MdatPhoneBillingCountryItems.objects.get(
                country_code=country_code
            )
        except MdatPhoneBillingCountryItems.DoesNotExist:
            continue

        # check if route exists
        try:
            route = MdatPhoneBillingRouteItems.objects.get(
                routing_key=country_code.country_code
            )
        except MdatPhoneBillingRouteItems.DoesNotExist:
            route = MdatPhoneBillingRouteItems(
                routing_key=country_code.country_code,
                item=country_code_billing.item_fixed_line,
            )
            route.save()

    return


@shared_task(name="mdat_phone_billing_minutes_picture_sync")
def mdat_phone_billing_minutes_picture_sync():
    picture_source = requests.get(
        "https://fonts.gstatic.com/s/i/short-term/release/materialsymbolsoutlined/phone_in_talk/default/48px.svg"
    )

    picture = svg2png(picture_source.content)

    items = (
        MdatItems.objects.filter(
            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
        )
        .filter(
            external_id__startswith="DTELMIN",
        )
        .filter(
            mdatitempictures__isnull=True,
        )
    )

    for item in items:
        new_picture = MdatItemPictures.objects.create(
            item=item,
            content=picture,
        )

    return


@shared_task(name="mdat_phone_billing_flatrates_picture_sync")
def mdat_phone_billing_flatrates_picture_sync():
    picture_source = requests.get(
        "https://fonts.gstatic.com/s/i/short-term/release/materialsymbolsoutlined/record_voice_over/default/48px.svg"
    )

    picture = svg2png(picture_source.content)

    items = (
        MdatItems.objects.filter(
            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
        )
        .filter(
            external_id__startswith="DTELFLAT",
        )
        .filter(
            mdatitempictures__isnull=True,
        )
    )

    for item in items:
        new_picture = MdatItemPictures.objects.create(
            item=item,
            content=picture,
        )

    return


@shared_task(name="mdat_phone_billing_access_product_picture_sync")
def mdat_phone_billing_access_product_picture_sync():
    picture_source = requests.get(
        "https://fonts.gstatic.com/s/i/short-term/release/materialsymbolsoutlined/sync_alt/default/48px.svg"
    )

    picture = svg2png(picture_source.content)

    items = (
        MdatItems.objects.filter(
            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
        )
        .filter(
            Q(external_id__startswith="DTELDSL")
            | Q(external_id__startswith="DTELFTTH"),
        )
        .filter(
            mdatitempictures__isnull=True,
        )
    )

    for item in items:
        new_picture = MdatItemPictures.objects.create(
            item=item,
            content=picture,
        )

    return
